FROM gcc
MAINTAINER Kasun De Zoysa
RUN apt-get update && apt-get install -y \
vim \
gdb \
git
RUN git config --global user.email "kasun@ucsc.cmb.ac.lk"
RUN git config --global user.name "kasund"
